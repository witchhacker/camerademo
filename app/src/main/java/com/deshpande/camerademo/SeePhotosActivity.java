package com.deshpande.camerademo;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class SeePhotosActivity extends AppCompatActivity {

    private ImageView imageView;
    private TextView file_name;

    ArrayList<HashMap<String,String>> list = new ArrayList<HashMap<String, String>>();

    private String JSON_STRING;
    private int j;

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_see_photos);

        j = 0;

        imageView = (ImageView) findViewById(R.id.photoView);
        file_name = (TextView) findViewById(R.id.fileName);
        getJSON();
    }

    private void showPhotos(){
        JSONObject jsonObject = null;
        try{
            jsonObject = new JSONObject(JSON_STRING);
            JSONArray result = jsonObject.getJSONArray(konfigurasi.TAG_JSON_ARRAY);

            for(int i=0;i<result.length();i++){
                JSONObject jo = result.getJSONObject(i);
                String nama = jo.getString(konfigurasi.TAG_nama);

                HashMap<String,String> photos = new HashMap<>();
                photos.put("",nama);
                list.add(photos);
            }
        }
        catch(JSONException je){
            je.printStackTrace();
        }

        String nama_file = list.get(j).toString()
                           .replace("=","")
                           .replace("{","")
                           .replace("}","")
                           .trim();
        file_name.setText(nama_file);
        Picasso.get().load("http://192.168.170.101/camera_android/Photos/" + nama_file).into(imageView);
    }

    private void getJSON(){
        class GetJSON extends AsyncTask<Void,Void,String>{
            ProgressDialog loading;

            @Override
            protected void onPreExecute(){
                super.onPreExecute();
                loading = ProgressDialog.show(SeePhotosActivity.this,"Mengambil Data","Foto sedang diambil dari server",false,false);
            }

            @Override
            protected void onPostExecute(String s){
                super.onPostExecute(s);
                loading.dismiss();
                JSON_STRING = s;
                showPhotos();
            }

            @Override
            protected String doInBackground(Void... params){
                RequestHandler rh = new RequestHandler();
                String s = rh.sendGetRequest(konfigurasi.URL_Tampil);
                return s;
            }
        }
        GetJSON gj = new GetJSON();
        gj.execute();
    }

    public void prev(View view){
        if(j > 0){
            j = j - 1;
        }
        else{
            j = 0;
        }
        getJSON();
    }

    public void next(View view){
        if(j < list.size() - 1){
            j = j + 1;
        }
        else{
            j = list.size() - 1;
        }
        getJSON();
    }
}
