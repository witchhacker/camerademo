package com.deshpande.camerademo;

import android.app.Activity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class ParseContent{
    private final String KEY_SUCCESS = "status";
    private final String KEY_MSG = "message";
    private Activity activity;

    ArrayList<HashMap<String,String>> arrayList;

    public ParseContent(Activity activity){
        this.activity = activity;
    }

    public boolean isSuccess(String response){
        try{
            JSONObject jsonObject = new JSONObject(response);
            if(jsonObject.optString(KEY_SUCCESS).equals("true")){
                return true;
            }
            else {
                return false;
            }
        }
        catch(JSONException je){
            je.printStackTrace();
        }
        return false;
    }

    public String getErrorCode(String response){
        try{
            JSONObject jsonObject = new JSONObject(response);
            return jsonObject.getString(KEY_MSG);
        }
        catch(JSONException je){
            je.printStackTrace();
        }
        return "No Data";
    }

    public String getURL(String response){
        String url = "";
        try{
            JSONObject jsonObject = new JSONObject(response);
            jsonObject.toString().replace("\\\\","");
            if(jsonObject.getString(KEY_SUCCESS).equals("true")){
                arrayList = new ArrayList<HashMap<String, String>>();
                JSONArray dataArray = jsonObject.getJSONArray("data");
                for(int i = 0;i < dataArray.length();i++){
                    JSONObject dataobj = dataArray.getJSONObject(i);
                    url = dataobj.optString("pathToFile");
                }
            }
        }
        catch(JSONException je){
            je.printStackTrace();
        }
        return url;
    }
}
