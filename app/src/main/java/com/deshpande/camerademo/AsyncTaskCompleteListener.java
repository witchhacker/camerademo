package com.deshpande.camerademo;

public interface AsyncTaskCompleteListener{
    void onTaskCompleted(String response,int serviceCode);
}
