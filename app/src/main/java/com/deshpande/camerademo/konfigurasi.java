package com.deshpande.camerademo;

public class konfigurasi{
    //Definisikan URL
    public static final String URL_Tambah = "http://192.168.170.101/camera_android/tambah_foto.php";
    public static final String URL_Tampil = "http://192.168.170.101/camera_android/tampil_foto.php";
    public static final String URL_Update = "http://192.168.170.101/camera_android/update_foto.php";
    public static final String URL_Hapus = "http://192.168.170.101/camera_android/hapus_foto.php?id=";

    //Key Data
    public static final String KEY_id = "id";
    public static final String KEY_nama = "nama";

    //JSON Tags
    public static final String TAG_JSON_ARRAY = "result";
    public static final String TAG_id = "id";
    public static final String TAG_nama = "nama";

    //ID Foto
    public static final String FOTO_id = "foto_id";
}
